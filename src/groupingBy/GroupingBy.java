package groupingBy;

import common.dto.Student;
import common.dto.Teacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingBy {

    public static void main(String[] args) {
        List<Teacher> dtos = new ArrayList<>();
        dtos.add(new Teacher(new Student("teacherid1", "studentId1")));
        dtos.add(new Teacher(new Student("teacherid1", "studentId2")));
        dtos.add(new Teacher(new Student("teacherid1", "studentId3")));
        dtos.add(new Teacher(new Student("teacherid2", "studentId1")));
        dtos.add(new Teacher(new Student("teacherid3", "studentId1")));


        // THIS WILL GROUP List<Teacher> to teacherId and store it in a map
        Map<String, List<Teacher>> collect = dtos.stream()
                .collect(
                        Collectors.groupingBy(
                                k -> k.getStudent().getTeacherId())
                );
        System.out.println(collect);

        // THIS WILL GROUP List<Student> to uiUniquePageKey and store it in a map
        Map<String, List<Student>> collect2 = dtos.stream()
                .map(Teacher::getStudent)
                .collect(Collectors.groupingBy(Student :: getTeacherId));
        System.out.println(collect2);


        /**
         * [NOT RECOMMENDED BY INTELLIJ]
         * as it leads to
         * CODE READABILITY PROBLEM
         */
        // THIS WILL GROUP List<String> (i.e. studentId) to teacherId and store it in a map
        Map<String, List<String>> collect3 = dtos.stream()
                .map(Teacher::getStudent)
                .collect(
                        Collectors.groupingBy(
                                Student::getTeacherId,
                                Collectors.mapping(Student::getStudentId, Collectors.toList())));
        System.out.println(collect3);


        /**
         * [RECOMMENDED BY INTELLIJ OVER GROUPING BY]
         * as it leads to
         * BETTER READABILITY
         */
        // THIS WILL GROUP List<String> (i.e. studentId) to teacherId and store it in a map
        Map<String, List<String>> collect4 = new HashMap<>();
        for (Teacher dto : dtos) {
            Student student = dto.getStudent();
            String studentId = student.getStudentId();
            collect4.computeIfAbsent(student.getTeacherId(), k -> new ArrayList<>())
                    .add(studentId);
        }
        System.out.println(collect4);
    }
}
