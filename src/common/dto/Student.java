package common.dto;

public class Student {

    public Student(String teacherId, String studentId) {
        this.teacherId = teacherId;
        this.studentId = studentId;
    }

    private String teacherId;
    private String studentId;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "teacherId='" + teacherId + '\'' +
                ", studentId='" + studentId + '\'' +
                '}';
    }
}
